<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Todo; //This syntax is to call model Todo
use\App\Category;
class TodoController extends Controller
{
    public function index(){

    	//belongs to --One in One to Many Relationship
    	//has many --Many in One to Many Relationship
    	$tasks = Todo::all(); //To call Todo model and to SELECT * FROM todos
    	//dd($tasks); //Dump and die, it's like var dump of PHP
    	return view('tasks', compact('tasks')); //compact('tasks') to call or publish the variable $tasks value
    }

    public function create(){
    	$categories = Category::all();
    	return view('add-task', compact('categories'));
    }

    public function store(Request $req){ //Pag data ay galing sa form kailangan ng Request
    	$new_task = new Todo; //new ModelName
    	$new_task->title = $req->title;
    	$new_task->body = $req->body;
    	$new_task->category_id = $req->category_id;
    	$new_task->status_id = 1;
    	$new_task->user_id = 1;

    	$new_task->save();
    	return redirect('/tasks');
    }

    public function destroy($id){ //si $id ay galing sa route url
    	$taskToDelete = Todo::find($id); //find the task to delete
    	$taskToDelete->delete(); //delete task
    	return redirect('/tasks'); //return to tasks url
    }

    public function markAsDone($id){
    	$taskToUpdate = Todo::find($id);

    	if($taskToUpdate->status_id ==3){
    		$taskToUpdate->status_id =1;
    	}
    	else{
    		$taskToUpdate->status_id =3;
    	}
    	

    	//$taskToUpdate->status_id = 3;
    	$taskToUpdate->save();
    	return redirect('/tasks');
    }
}
