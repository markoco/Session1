<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//To show all tasks
Route::get('/tasks', 'TodoController@index');

//To show add task form
Route::get('/addtask', 'TodoController@create');
//To save new task
Route::post('/addtask', 'TodoController@store');

Route::delete('/deletetask/{id}', 'TodoController@destroy');

Route::patch('/markasdone/{id}', 'TodoController@markAsDone');