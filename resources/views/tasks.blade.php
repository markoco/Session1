@extends("layouts.app")
@section("content")

<h1 class="text-center py-5">TODO LIST</h1>
<div class="row">
	{{-- Controllers Data Can be accessed here --}}
	@foreach($tasks as $indiv_task)
		<div class="col-lg-3 my-2">
			<div class="card">
				<div class="card-body">
					{{-- the {{}} is the counterpart of echo while thin arrow is the bracket--}}
					<h4 class="card-title">{{$indiv_task->title}}</h4>
					<p class="card-text">{{$indiv_task->body}}</p>
					<p class="card-text">{{$indiv_task->category->name}}</p>{{-- You must connect the foreign key via Model--}}
					<p class="card-text">{{$indiv_task->status->name}}</p>
				</div>
				<div class="card-footer">
					<form action="/deletetask/{{$indiv_task->id}}" method="POST">
						@csrf
						@method('DELETE')
						<button class="btn btn-danger" type="submit">Delete Task</button>
					</form>	
					<form action="/markasdone/{{$indiv_task->id}}" method="POST">
						@csrf
						@method('PATCH')
						<button class="btn btn-success" type="submit">
							@if($indiv_task->status_id != 1)
								<span>Mark as Pending</span>
							@else()
								<span>Mark as Done</span>
							@endif
						</button>
					</form>
				</div>
			</div>
		</div>
	@endforeach	
</div>


@endsection